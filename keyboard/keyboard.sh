#!/bin/bash

set -e

WAVE=${1-sine}
NOTE_DURATION=${2-0.2}

NOTES=("523.3" "554.4" "587.3" "622.3" "659.3" "698.5" "740.0" "784.0" "830.6" "880.0" "932.3" "987.8" "1047")

SOX_COMMAND="play -n -n synth $NOTE_DURATION $WAVE"

while [ 1 ] ; do
	read -rsn1 INPUT &> /dev/null

	case $INPUT in
		q)
			$($SOX_COMMAND ${NOTES[0]} &> /dev/null)
			;;
		2)
			$($SOX_COMMAND ${NOTES[1]} &> /dev/null)
			;;
		w)
			$($SOX_COMMAND ${NOTES[2]} &> /dev/null)
			;;
		3)
			$($SOX_COMMAND ${NOTES[3]} &> /dev/null)
			;;
		e)
			$($SOX_COMMAND ${NOTES[4]} &> /dev/null)
			;;
		r)
			$($SOX_COMMAND ${NOTES[5]} &> /dev/null)
			;;
		5)
			$($SOX_COMMAND ${NOTES[6]} &> /dev/null)
			;;
		t)
			$($SOX_COMMAND ${NOTES[7]} &> /dev/null)
			;;
		6)
			$($SOX_COMMAND ${NOTES[8]} &> /dev/null)
			;;
		y)
			$($SOX_COMMAND ${NOTES[9]} &> /dev/null)
			;;
		7)
			$($SOX_COMMAND ${NOTES[10]} &> /dev/null)
			;;
		u)
			$($SOX_COMMAND ${NOTES[11]} &> /dev/null)
			;;
		i)
			$($SOX_COMMAND ${NOTES[12]} &> /dev/null)
			;;
	esac
done
