# Keyboard.sh

Turns "q2w3er5t6y7ui" into a monophonic keyboard beginning at C5 and going to C6. Since SoX takes time to startup and shutdown, this also cuts all the notes into short samples, with gaps in between. Sounds like bad chiptune...

Dependencies: 
* BASH
* SoX

usage: ./keyboard.sh $waveformtype $noteduration.

_possible waveforms:_ sine, square, triangle, sawtooth, trapezium, exp, noise, tpdfnoise, pinknoise, brownnoise, pluck.

ex: ./keyboard pluck 0.5

__ctrl-c quits.__

## Notes:

bash is slow, which is why people don't really use it for sound. Also, the latency introduced by constantly rerunning sox is a bit of a pain.

Kinda neat though!
