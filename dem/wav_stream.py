#!/bin/env python3

import sys
import wave as wav

class wav_stream:
    def __init__(self, filename):
        # The following code cribbed from pdx-cs-sound/findpeak.py,
        # so thank you, Bart!
        self.wavfile = wav.open(filename, 'rb')
        self.channels = self.wavfile.getnchannels()
        self.width = self.wavfile.getsampwidth()
        self.rate = self.wavfile.getframerate()
        self.frames = self.wavfile.getnframes()
        self.frame_width = self.width * self.channels
        self.wavbytes = self.wavfile.readframes(self.frames)

    def locate_peaks(self):
        max_sample = None
        min_sample = None
        print("locate_peaks")
        for f in range(0, len(self.wavbytes), self.frame_width):
            # slicing the bytes to get just a single frame.
            frame = self.wavbytes[f : f + self.frame_width]
            # Iterate over channels.
            for c in range(0, len(frame), self.width):
            # Build a sample.
                sample_bytes = frame[c : c + self.width]

                # Eight-bit samples are unsigned
                sample = int.from_bytes(sample_bytes,
                                byteorder='little',
                                signed=(self.width>1))

                # Check extrema.
                if max_sample is None:
                    max_sample = sample
                if min_sample is None:
                    min_sample = sample
                if sample > max_sample:
                    max_sample = sample
                if sample < min_sample:
                    min_sample = sample

                print("max: %s", max_sample)
                print("min: %s", max_sample)

                print("\n")

    def wavclose(self):
        self.wavfile.close()
